import { Injectable } from '@angular/core';
import {Hero} from "./hero";
import {Heroes} from "../mock-hereoes"
import { Observable, of } from 'rxjs';
import { MessageService } from "./message.service";
@Injectable({
  providedIn: 'root'
})
export class HeroService {

  getHeroes(): Observable<Hero[]>{
    this.messageService.addMessage('HeroService: fetched Heroes');
    return of (Heroes);
  }
  constructor(private messageService : MessageService) { }
}
