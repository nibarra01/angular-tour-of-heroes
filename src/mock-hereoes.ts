import { Hero } from './app/hero';

export const Heroes: Hero[] = [
    {
        id: 1,
        name: 'Batman'
    },
    {
        id: 2,
        name: 'Green Lantern'
    },
    {
        id: 3,
        name: 'Captain  America'
    },
    {
        id: 4,
        name: 'Superman'
    },
    {
        id: 5,
        name: 'Iron Man'
    },
    {
        id: 6,
        name: 'The Flash'
    },
    {
        id: 7,
        name: 'Thor'
    },
    {
        id: 8,
        name: 'Spider Man'
    },
    {
        id: 9,
        name: 'Wolverine'
    },
    {
        id: 10,
        name: 'Booster Gold'
    },
]